
from missiles import *

class Player():
    
    points = 0

    def __init__(self, width, height, posx, posy):
        self.width = width
        self.height = height
        self.posx = posx
        self.posy = posy
        
        self.cooldown = 0
        self.hp = 4
    
    def shoot(self):
        #SOUNDS['shoot'].play()
        new_bullet = Bullet(self.posx+ (self.width/2), self.posy + self.height, False)
            
    def move(self, x, y):
        
        self.posx += x
        self.posy += y
        
    def draw(self):
        if self.cooldown > 0:
            self.cooldown += -1

        pygame.draw.rect(screen, (255,255,255), (self.posx, self.posy, self.width, self.height))
    
    def check_collision(self):
        
        if all_bullets:

            for index, bullet in enumerate(all_bullets):
                    
                if (bullet.posy + bullet.height >= self.posy > bullet.posy) and (bullet.isEnemy == True) and (bullet.posx + bullet.width > self.posx > bullet.posx):
                                 
                    all_bullets.pop(index)
                    self.width = self.width * 0.8
                    self.height = self.height * 0.8
                    self.hp -= 1
    
    def update(self,directionX, directionY):
        
        self.move(directionX, directionY)
        self.check_collision()
        self.draw()
    
        
class Enemy():

    def __init__(self, size, posx, posy):

        self.size = size
        self.color = (255,255,255)

        self.posx = posx
        self.posy = posy
        
        self.moving_right = True
        self.speed = 5
        self.counter = 0
        self.hp = 4
        
    def collide(self, bullet):

        self.size[0] = self.size[0] * 0.9
        self.size[1] = self.size[1] * 0.9    
        
        self.color = random.choice((color1, color2, color3, color4, color5))
        self.speed += 5
        self.counter += 10
        Player.points += 1
        
    def check_collision(self):
        
        if all_bullets:

            for index, bullet in enumerate(all_bullets):
                    
                if bullet.isEnemy == False:
                    
                    if (self.posy < bullet.posy <= self.posy + self.size[1]) and self.posx < bullet.posx < self.posx + self.size[0]:
                        all_bullets.pop(index)
                        self.collide(bullet)
                
                    
    def move(self):
    
        if (self.moving_right) and self.posx + self.size[0] >= width:
            self.moving_right = False
        
        elif (self.moving_right == False) and self.posx <= 0:
            self.moving_right = True
            
        if self.moving_right:
            self.posx += self.speed
            
        else:
            self.posx -= self.speed
        
    def draw(self):
        pygame.draw.rect(screen, self.color, (self.posx, self.posy, self.size[0], self.size[1]))
    
    def shoot(self):
        
        new_bullet = Bullet(self.posx+ (self.size[0]/2), self.posy + self.size[1], True)
    
    def update(self):
        
        if self.counter >= 60:
            self.shoot()
            self.counter = 0
        
        self.check_collision()
        self.move()
        self.draw()
        self.counter += 1