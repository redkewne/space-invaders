import pygame
from pygame.locals import *
import random

all_bullets = []

pygame.init()

pygame.mouse.set_visible(False)

width = 1900
height = 1080

screen = pygame.display.set_mode((width, height), FULLSCREEN)

pygame.display.set_caption('Galaga Game WIP')

SOUNDS = {}

#SOUNDS['shoot'] = pygame.mixer.Sound('shot2.wav')

color1 = (125,100,55)
color2 = (200,0,70)
color3 = (50,200,120)
color4 = (220, 220, 100)
color5 = (240, 150, 240)

all_bullets = []

class Bullet():
    
    def __init__(self, posx, posy, isEnemy, isDebris = False):
        
        self.posx = posx
        self.posy = posy
        self.isEnemy = isEnemy
        
        self.width = 10
        self.height = 10
        self.color = (255,255,255)
        
        self.isDebris = isDebris
        
        all_bullets.extend([self])
        
    def move(self):
        if self.isEnemy:
            self.posy += 10
        else:
            self.posy -= 20
    
    def inside_universe(self):
        
        if (self.posy + self.height < 0) or self.posy > height:
            return False
        else:
            return True
    
    def create_debris(self):
        
        if self.counter == 60:
            self.counter = 0
            new_debris = [Bullet(self.posx/2, self.posy/2, isEnemy = True, isDebris = True)]
    
    def increase_size(self):
        
        if self.isEnemy:
            self.width += 2
            self.height += 2
    
    def draw(self):
        self.color = random.choice((color1,color2,color3,color4,color5))
        pygame.draw.rect(screen, self.color, (self.posx, self.posy, self.width, self.height))
    
    def update(self):
    
        if self.inside_universe():
            self.move()
            self.increase_size()
            self.draw()
        
        else:
            all_bullets.remove(self)