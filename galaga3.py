from units import *


square = Player(45,25,int(width/2)- int(45),800)        

ENEMIES = [Enemy([100,100], 0,125),
        Enemy([50,50],500,200),
        Enemy([100,100],300,300),
        Enemy([50,50], 700, 400),
		Enemy([100,100], 0,0)]


def main():

    clock = pygame.time.Clock()
    running = True

    while running:
        
        screen.fill((15, 15, 15))
        
        for event in pygame.event.get():
            
            dx = 0
            dy = 0

            if (event.type == QUIT) or square.hp == 0:
                running = False
                
            keys = pygame.key.get_pressed()
    
            if keys[pygame.K_LEFT]:
                dx = -15
            if keys[pygame.K_RIGHT]:
                dx = 15
            if keys[pygame.K_UP]:
                dy = -15
            if keys[pygame.K_DOWN]:
                dy = 15
            if keys[pygame.K_SPACE] and square.cooldown == 0:
                square.shoot()
                square.cooldown = 5
            if keys[pygame.K_ESCAPE]:
                running = False
        
        square.update(dx,dy)
        
        for enemy in ENEMIES:
            enemy.update()
        
        for bullet in all_bullets:
            bullet.update()
        
        pygame.display.flip()
        clock.tick(60)

    pygame.quit()
    print('Oops, you either died or you closed this window!' + '\n' + 'You scored', Player.points, 'points!')

main()